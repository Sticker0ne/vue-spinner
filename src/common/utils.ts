export async function setAsyncTimer(payload: { time: number }) {
  return new Promise<void>(resolve => {
    setTimeout(resolve, payload.time);
  });
}

export function jsonClone<T>(payload: T): T {
  return JSON.parse(JSON.stringify(payload));
}
