import { setAsyncTimer } from "@/common/utils";

export interface IUser {
  id: number;
  firstName: string;
  lastName: string;
  bookAmount?: number;
}

export function useDataMocks() {
  const loadUsersData = async (): Promise<IUser[]> => {
    await setAsyncTimer({ time: 2000 });
    return [
      { id: 1, firstName: "Serega", lastName: "Mirtov" },
      { id: 2, firstName: "Ann", lastName: "Mari" },
      { id: 3, firstName: "Alex", lastName: "Karakulov", bookAmount: 120 },
    ];
  };

  const loadUserData = async (payload: IUser): Promise<IUser> => {
    await setAsyncTimer({ time: 2000 });
    return { bookAmount: 300, ...payload };
  };

  return {
    loadUsersData,
    loadUserData,
  };
}
